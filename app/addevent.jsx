import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Alert, ImageBackground, Platform, TouchableOpacity } from 'react-native';
import { collection, addDoc } from 'firebase/firestore';
import { db } from '../Firebase/firebase-config';
import DateTimePicker from '@react-native-community/datetimepicker';
import MapView, { Marker } from 'react-native-maps';

const AddEvent = () => {
  const [title, setTitle] = useState('');
  const [price, setPrice] = useState('');
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [location, setLocation] = useState(null);

  const handleSubmit = async () => {
    if (!title || !price || !date || !time || !location) {
      Alert.alert('Error', 'Please fill in all fields.');
      return;
    }

    try {
      const timestamp = new Date(date);
      timestamp.setHours(time.getHours());
      timestamp.setMinutes(time.getMinutes());

      await addDoc(collection(db, "events"), { title, price, timestamp, location });
      Alert.alert('Success', 'Event added successfully!');
      setTitle('');
      setPrice('');
      setDate(new Date());
      setTime(new Date());
      setLocation(null);
    } catch (error) {
      console.error('Error adding event:', error);
      Alert.alert('Error', 'Failed to add event. Please try again later.');
    }
  };

  return (
    <ImageBackground source={require('../assets/pozadina.png')} style={styles.background}>
      <View style={styles.overlay}>
        <Text style={styles.mainTitle}>EventsForEase</Text>
        <View style={styles.container}>
          <Text style={styles.label}>Event Name:</Text>
          <TextInput
            style={styles.input}
            value={title}
            onChangeText={setTitle}
            placeholder="Enter event name"
            placeholderTextColor="#999"
          />
          <Text style={styles.label}>Date:</Text>
          <TouchableOpacity onPress={() => setShowDatePicker(true)} style={styles.datePicker}>
            <Text style={styles.datePickerText}>{formatDate(date)}</Text>
          </TouchableOpacity>
          {showDatePicker && (
            <DateTimePicker
              value={date}
              mode="date"
              display="default"
              onChange={(event, selectedDate) => {
                const currentDate = selectedDate || date;
                setShowDatePicker(Platform.OS === 'ios');
                setDate(currentDate);
              }}
            />
          )}
          <Text style={styles.label}>Start Time:</Text>
          <TouchableOpacity onPress={() => setShowTimePicker(true)} style={styles.datePicker}>
            <Text style={styles.datePickerText}>{formatTime(time)}</Text>
          </TouchableOpacity>
          {showTimePicker && (
            <DateTimePicker
              value={time}
              mode="time"
              display="default"
              onChange={(event, selectedTime) => {
                const currentTime = selectedTime || time;
                setShowTimePicker(Platform.OS === 'ios');
                setTime(currentTime);
              }}
            />
          )}
          <Text style={styles.label}>Price:</Text>
          <TextInput
            style={styles.input}
            value={price}
            onChangeText={setPrice}
            placeholder="Enter price"
            placeholderTextColor="#999"
            keyboardType="numeric"
          />
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              initialRegion={{
                latitude: 45.5549,
                longitude: 18.6953,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              onPress={(e) => setLocation(e.nativeEvent.coordinate)}
            >
              {location && <Marker coordinate={location} />}
            </MapView>
          </View>
          <TouchableOpacity style={styles.submitButton} onPress={handleSubmit}>
            <Text style={styles.submitButtonText}>Create Event</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

const formatDate = (date) => {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  return `${day}/${month}/${year}`;
};

const formatTime = (time) => {
  const hours = time.getHours();
  const minutes = time.getMinutes();
  return `${hours}:${minutes < 10 ? '0' + minutes : minutes}`;
};

export default AddEvent;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
  },
  overlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  mainTitle: {
    marginTop: 27,
    color: 'white',
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
  },
  container: {
    width: '90%',
    padding: 20,
    borderRadius: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  label: {
    fontSize: 18,
    color: 'white',
    marginBottom: 10,
  },
  input: {
    height: 40,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: '#d3d3d3',
    marginBottom: 15,
    paddingHorizontal: 10,
    borderRadius: 5,
    color: '#000',
  },
  datePicker: {
    height: 40,
    justifyContent: 'center',
    backgroundColor: '#d3d3d3',
    marginBottom: 15,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  datePickerText: {
    color: '#000',
  },
  mapContainer: {
    height: 200,
    marginBottom: 20,
  },
  map: {
    flex: 1,
  },
  submitButton: {
    height: 50,
    backgroundColor: '#28a745',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  submitButtonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
