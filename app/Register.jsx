import { StyleSheet, View, Text, TextInput, Button, Alert, ImageBackground } from 'react-native';
import React, { useState } from 'react';
import { authentication } from '../Firebase/firebase-config';
import { createUserWithEmailAndPassword } from 'firebase/auth';

const Register = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleCreateAccount = async () => {
    console.log('Creating account with:', { email, password });

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(email)) {
      Alert.alert('Invalid Email', 'Please enter a valid email address.');
      console.log('Invalid Email');
      return;
    }

    if (password.length < 6 || password.length > 15) {
      Alert.alert('Invalid Password', 'Password must be 6 to 15 characters long.');
      console.log('Invalid Password Length');
      return;
    }

    try {
      await createUserWithEmailAndPassword(authentication, email, password);
      console.log('User created:');

      Alert.alert('Account Created', `Email: ${email}`);
      console.log('Account Created:', { email, password });
    } catch (error) {
      if (error.code === 'auth/email-already-in-use') {
        Alert.alert('Email In Use', 'This email is already associated with an existing account.');
        console.log('Email is already in use');
      } else {
        Alert.alert('Error', error.message);
        console.error('Error creating account:', error);
      }
    }
  };

  return (
    <ImageBackground source={require('../assets/pozadina.png')} style={styles.background}>
      <Text style={styles.topTitle}>EventsForEase</Text>
      <View style={styles.container}>
        <Text style={styles.title}>Create Account</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
          secureTextEntry
        />
        <Button title="Register" onPress={handleCreateAccount} />
      </View>
    </ImageBackground>
  );
};

export default Register;

const styles = StyleSheet.create({
  topTitle: {
    position: 'absolute',
    top: 80, 
    fontSize: 32,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)', 
    padding: 40, 
    borderRadius: 20, 
    width: '80%', 
    maxWidth: 400, 
  },
  title: {
    fontSize: 30, 
    marginBottom: 10,
    textAlign: 'center',
  },
  input: {
    height: 50, 
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20, 
    paddingHorizontal: 15, 
    borderRadius: 10, 
    backgroundColor: 'white',
    fontSize: 16, 
  },
});