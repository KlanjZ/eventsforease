import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground, ScrollView, Dimensions, TouchableOpacity } from 'react-native';
import { authentication, db } from '../Firebase/firebase-config';
import { collection, getDocs, doc, query, getDoc } from 'firebase/firestore';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

const EventsForEase = () => {
  const navigation = useNavigation();
  const [userEmail, setUserEmail] = useState(null);
  const [userUid, setUserUid] = useState(null);
  const [savedEventsDetails, setSavedEventsDetails] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    const unsubscribeAuth = authentication.onAuthStateChanged(user => {
      if (user) {
        setUserEmail(user.email);
        setUserUid(user.uid);
        fetchUserData(); 
      } else {
        setUserEmail(null);
        setUserUid(null);
        setSavedEventsDetails([]);
        setRefreshing(false);
        navigation.navigate('sign'); 
      }
    });

    return () => unsubscribeAuth();
  }, [navigation]);

  const fetchUserData = async () => {
    const currentUser = authentication.currentUser;

    const userRef = doc(db, 'users', currentUser.uid);
    const savedEventsQuery = query(collection(userRef, 'savedEvents'));
    const querySnapshot = await getDocs(savedEventsQuery);

    const eventIdsSet = new Set();
    querySnapshot.forEach((doc) => {
      const eventData = doc.data();
      if (eventData.eventId) {
        eventIdsSet.add(eventData.eventId);
      }
    });

    const eventIds = Array.from(eventIdsSet);
    const eventsDetails = [];
    for (const eventId of eventIds) {
      const eventDocRef = doc(db, 'events', eventId);
      const eventDocSnapshot = await getDoc(eventDocRef);
      if (eventDocSnapshot.exists()) {
        const eventData = eventDocSnapshot.data();
        const eventDate = eventData.timestamp.toDate();
        if (eventDate > new Date()) {
          eventsDetails.push({
            id: eventId,
            title: eventData.title,
            coordinates: eventData.location,
            price: eventData.price,
            date: eventDate,
          });
        }
      }
    }

    eventsDetails.sort((a, b) => a.date - b.date);
    setSavedEventsDetails(eventsDetails);
    setRefreshing(false);
  };

  const handleRefresh = () => {
    setRefreshing(true);
    fetchUserData();
  };

  const renderEvent = ({ item }) => (
    <View style={styles.eventContainer}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.eventMap}
        initialRegion={{
          latitude: item.coordinates.latitude,
          longitude: item.coordinates.longitude,
          latitudeDelta: 0.03,
          longitudeDelta: 0.03,
        }}
        scrollEnabled={false}
        zoomEnabled={false}
        liteMode={true}
      >
        <Marker coordinate={item.coordinates} />
      </MapView>
      <View style={styles.eventDetails}>
        <Text style={styles.eventTitle}>{item.title}</Text>
        <Text>{item.price}€</Text>
        <Text>{item.date ? `Date: ${item.date.toLocaleDateString()}` : ''}</Text>
        <Text>{item.date ? `Time: ${item.date.toLocaleTimeString()}` : ''}</Text>
      </View>
    </View>
  );

  return (
    <ImageBackground source={require('../assets/pozadina.png')} style={styles.background}>
      <View style={styles.container}>
        <Text style={styles.header}>EventsForEase</Text>
        {userEmail ? (
          <View style={styles.userInfoContainer}>
            <Text style={[styles.text, styles.userInfoText]}>Hello, {userEmail}</Text>
            <Text style={styles.text}>Saved Events:</Text>
            <TouchableOpacity onPress={handleRefresh} style={styles.refreshButton}>
              <Text style={styles.refreshButtonText}>Refresh</Text>
            </TouchableOpacity>
            <ScrollView contentContainerStyle={styles.scrollContainer}>
              {savedEventsDetails.length > 0 ? (
                savedEventsDetails.map((event) => (
                  <View key={event.id} style={styles.eventWrapper}>
                    {renderEvent({ item: event })}
                  </View>
                ))
              ) : (
                <Text style={styles.text}>No saved events</Text>
              )}
            </ScrollView>
          </View>
        ) : (
          <Text style={styles.text}>No user signed in</Text>
        )}
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 50,
    marginBottom: 60,
  },
  header: {
    fontSize: 32,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'white',
  },
  text: {
    fontSize: 24,
    marginBottom: 10,
    color: 'white',
  },
  scrollContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventWrapper: {
    width: width * 0.9,
    marginBottom: 20,
  },
  eventContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 10,
    borderRadius: 5,
    width: width * 0.9,
  },
  eventMap: {
    width: 100,
    height: 100,
    marginRight: 10,
    borderRadius: 5,
  },
  eventDetails: {
    flex: 1,
  },
  eventTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  userInfoContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  userInfoText: {
    marginBottom: 10,
  },
  refreshButton: {
    backgroundColor: '#4CAF50',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginBottom: 10,
  },
  refreshButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default EventsForEase;
