import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, FlatList, StyleSheet, Alert, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';
import * as Location from 'expo-location';
import haversine from 'haversine';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { collection, getDocs, addDoc, doc } from 'firebase/firestore';
import { db } from '../Firebase/firebase-config';
import { Ionicons } from '@expo/vector-icons';
import AddEventScreen from './addevent'; 
import ProfileScreen from './profile'; 
import { authentication } from '../Firebase/firebase-config';

const EventScreenComponent = ({ userUid }) => {
  const [radius, setRadius] = useState('');
  const [events, setEvents] = useState([]);
  const [filteredEvents, setFilteredEvents] = useState([]);
  const [location, setLocation] = useState(null);
  const [loading, setLoading] = useState(true);
  const [fetchingEvents, setFetchingEvents] = useState(false);

  useEffect(() => {
    const getLocation = async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert('Permission to access location was denied');
        setLoading(false);
        return;
      }

      let currentLocation = await Location.getCurrentPositionAsync({});
      setLocation({
        latitude: currentLocation.coords.latitude,
        longitude: currentLocation.coords.longitude,
      });

      setLoading(false);
    };

    getLocation();
  }, []);

  const fetchEvents = async () => {
    try {
      setFetchingEvents(true);
      const querySnapshot = await getDocs(collection(db, 'events'));
      const fetchedEvents = [];
      const currentDate = new Date(); 

      querySnapshot.forEach(doc => {
        const data = doc.data();
        const eventDate = new Date(data.timestamp.seconds * 1000);

        if (eventDate > currentDate) {
          const distance = haversine(
            { latitude: location.latitude, longitude: location.longitude },
            { latitude: data.location.latitude, longitude: data.location.longitude },
            { unit: 'meter' }
          );
          fetchedEvents.push({
            id: doc.id,
            title: data.title,
            price: `${data.price}€ - ${Math.round(distance)}m`,
            date: eventDate.toISOString().split('T')[0],
            time: eventDate.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' }),
            coordinates: {
              latitude: data.location.latitude,
              longitude: data.location.longitude,
            }
          });
        }
      });

      fetchedEvents.sort((a, b) => {
        return new Date(a.date) - new Date(b.date);
      });

      setFetchingEvents(false);
      return fetchedEvents;
    } catch (error) {
      console.error('Error fetching events: ', error);
      setFetchingEvents(false);
    }
  };

  const handleApply = async () => {
    if (!location) {
      Alert.alert('Error', 'Location not available');
      return;
    }

    const radiusInKm = parseFloat(radius);
    if (isNaN(radiusInKm)) {
      Alert.alert('Invalid Radius', 'Please enter a valid radius in km.');
      return;
    }

    const fetchedEvents = await fetchEvents();
    if (fetchedEvents) {
      const filtered = fetchedEvents.filter(event => {
        const distance = haversine(location, event.coordinates);
        return distance <= radiusInKm;
      });

      setEvents(fetchedEvents);
      setFilteredEvents(filtered);
    }
  };

  const handleSaveEvent = async (eventId, eventTitle) => {
    try {
      const userDocRef = doc(db, 'users', userUid); 
      await addDoc(collection(userDocRef, 'savedEvents'), {
        eventId: eventId,
        eventTitle: eventTitle,
      });
      Alert.alert('Event Saved', `You have saved ${eventTitle}`);
    } catch (error) {
      console.error('Error saving event: ', error);
      Alert.alert('Error', 'Failed to save event. Please try again.');
    }
  };

  const renderEvent = ({ item }) => (
    <View style={styles.eventContainer}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.eventMap}
        initialRegion={{
          latitude: item.coordinates.latitude,
          longitude: item.coordinates.longitude,
          latitudeDelta: 0.03,
          longitudeDelta: 0.03,
        }}
        scrollEnabled={false}
        zoomEnabled={false}
        liteMode={true}
      >
        <Marker coordinate={item.coordinates} />
      </MapView>
      <View style={styles.eventDetails}>
        <Text style={styles.eventTitle}>{item.title}</Text>
        <Text>{item.price}</Text>
        <Text>{`Date: ${item.date}, Time: ${item.time}`}</Text>
        <Button title="Save event" onPress={() => handleSaveEvent(item.id, item.title)} />
      </View>
    </View>
  );

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#0000ff" />
        <Text>Loading...</Text>
      </View>
    );
  }

  return (
    <ImageBackground source={require('../assets/pozadina.png')} style={styles.background}>
      <View style={styles.container}>
        <Text style={styles.title}>EventsForEase</Text>
        <View style={styles.distanceContainer}>
          <Text style={styles.choose}>Choose distance:</Text>
          <TextInput
            style={styles.input}
            placeholder="Distance in km..."
            value={radius}
            onChangeText={setRadius}
            keyboardType="numeric"
          />
          <Button title="Apply" onPress={handleApply} />
        </View>
        {fetchingEvents ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
          <>
            <Text style={styles.subtitle}>Nearby Events</Text>
            <FlatList
              data={filteredEvents}
              renderItem={renderEvent}
              keyExtractor={item => item.id}
            />
          </>
        )}
      </View>
    </ImageBackground>
  );
};

const Tab = createBottomTabNavigator();

const EventScreen = () => {
  const [userUid, setUserUid] = useState(null);

  useEffect(() => {
    const fetchUserData = async () => {
      if (authentication.currentUser) {
        setUserUid(authentication.currentUser.uid);
      } else {
        setUserUid(null);
      }
    };

    fetchUserData()});
  
  

  return (
    <NavigationContainer independent={true}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Events') {
              iconName = focused ? 'list' : 'list';
            } else if (route.name === 'Add Event') {
              iconName = focused ? 'add-circle' : 'add-circle-outline';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'person' : 'person-outline';
            }

            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
        })}
      >
        <Tab.Screen name="Events">
          {() => <EventScreenComponent userUid={userUid} />}
        </Tab.Screen>
        <Tab.Screen name="Profile" component={ProfileScreen} />
        <Tab.Screen name="Add Event" component={AddEventScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default EventScreen;

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: 30,
    color: 'white',
  },
  choose: {
    color: 'white',
  },
  distanceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    marginRight: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    backgroundColor: 'lightgray',
  },
  subtitle: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: 'white',
  },
  eventContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
  },
  eventMap: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  eventDetails: {
    marginLeft: 10,
    flex: 1,
    justifyContent: 'center',
  },
  eventTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});