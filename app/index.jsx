import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import { Link } from 'expo-router';

export default function App() {
  return (
    <ImageBackground 
      source={require('../assets/pozadina.png')}
      style={styles.background}
    >
      <View style={styles.container}>
        <Text style={styles.title}>EventsForEase</Text>
        
        <Image 
          source={require('../assets/person.png')}
          style={styles.icon}
        />
        
        <View style={styles.buttonContainer}>
          <Link href="/sign" style={styles.button}>
            <Text style={styles.buttonText}>Login</Text>
          </Link>
          <Link href="/Register" style={styles.button}>
            <Text style={styles.buttonText}>Register</Text>
          </Link>
        </View>
        
        <StatusBar style="auto" />
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    position: 'absolute',
    top: 80, 
    fontSize: 32,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  icon: {
    marginTop: 280,
    width: 220, 
    height: 220, 
    marginVertical: 20, 
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 120, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#800000', 
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 25,
    marginHorizontal: 10,
    alignItems: 'center', 
    justifyContent: 'center', 
    width: 120, 
    textAlign: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center', 
  },
});

