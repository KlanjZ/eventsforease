import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TextInput, Button, Alert, ImageBackground } from 'react-native';
import { authentication } from '../Firebase/firebase-config';
import { signInWithEmailAndPassword, onAuthStateChanged } from 'firebase/auth';
import { useNavigation } from '@react-navigation/native'; 

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation(); 

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(authentication, user => {
      if (user) {
        navigation.navigate('event'); 
      }
    });

    return unsubscribe; 
  }, []);

  const handleLogin = async () => {
    console.log('Logging in with:', { email, password });

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(email)) {
      Alert.alert('Invalid Email', 'Please enter a valid email address.');
      console.log('Invalid Email');
      return;
    }

    try {

      await signInWithEmailAndPassword(authentication, email, password);
      console.log('User logged in:', { email });
    
      navigation.navigate('event'); 
    
      Alert.alert('Login Successful', `Welcome back, ${email}`);
    } catch (error) {
      if (error.code === 'auth/invalid-credential') {
        Alert.alert('User Not Found', 'There is no user corresponding to the given email address or the password you entered is incorrect.');
      } else {
        Alert.alert('Error', 'An error occurred while logging in. Please try again later.');
      }
    }
  };

  return (
    <ImageBackground source={require('../assets/pozadina.png')} style={styles.background}>
      <Text style={styles.topTitle}>EventsForEase</Text>
      <View style={styles.container}>
        <Text style={styles.title}>Login</Text>
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
          secureTextEntry
        />
        <Button title="Login" onPress={handleLogin} style={styles.button} />
      </View>
    </ImageBackground>
  );
};

export default Login;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topTitle: {
    position: 'absolute',
    top: 80, 
    fontSize: 32,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  container: {
    width: '80%',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  title: {
    fontSize: 28,
    marginBottom: 20,
    textAlign: 'center',
  },
  input: {
    height: 50,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingHorizontal: 15,
    borderRadius: 10,
    backgroundColor: 'white',
    width: '100%',
  },
  button: {
    width: '100%',
    borderRadius: 10,
  },
});