import { initializeApp } from 'firebase/app';
import { initializeAuth, getReactNativePersistence } from 'firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getFirestore} from 'firebase/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyD-LFi4ZYky85V7Vuto6IWpqDJcAUzunK4",
  authDomain: "eventsforease-b16f6.firebaseapp.com",
  projectId: "eventsforease-b16f6",
  storageBucket: "eventsforease-b16f6.appspot.com",
  messagingSenderId: "692580816841",
  appId: "1:692580816841:web:af3c279821d0ee25cb7a18"
};

const app = initializeApp(firebaseConfig);

const authentication = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage)
});


const db = getFirestore(app);

export { authentication, db };